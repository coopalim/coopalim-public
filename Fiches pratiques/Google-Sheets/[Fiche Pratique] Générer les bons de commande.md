# [Fiche Pratique] <GENERER DES BONS DE COMMANDE COOPALIM AVEC GOOGLE-SHEETS>

[//]: # (Version Canevas : 0.2)

# Description

Dans cette fiche pratique, vous allez apprendre à :
 * Préparer des données pour la génération des bons de commande/livraison
 * Générer les bons individuels pour chaque adhérent/utilisateur ayant commandé

## Prérequis
 * [Fiche Pratique] Installer un addon Google Sheets
 * Avoir accès à un fichier de commandes déjà renseigné
 * Avoir accès à un fichier de modèle de bon de commande

 
# Procédure


## [1/4] <INSTALLATION DE L'ADDON Coopalim>
### Aller sur cette page chrome.google.com/webstore/detail/coopalim-google-sheets-ut/ildhjhmmpbffblmfbpdhcnbalpejebbj
et installer l'addon. Cela renvoie sur une google sheets vide. 
Une fenêtre de demande d'autorisation de l'add-on devrait s'afficher. Il faudra cliquer sur "Continue".
![TBD](images/cplm_addon-coopalim_enable.png "")

### Autoriser un add-on à s'exécuter dans votre Google Sheet
!! Vous aurez sans doute des alertes de sécurité car le script n'est pas validé par Google (et pour cause).
Suivez la procédure ci-après pour l'autoriser
![TBD](images/cplm_authorize_gscript_auth-required.png "")
![TBD](images/cplm_authorize_gscript_choose-account.png "")
![TBD](images/cplm_authorize_gscript_go-to-advanced.png "")
![TBD](images/cplm_authorize_gscript_allow-unsafe.png "")
![TBD](images/cplm_authorize_gscript_grant-persmissions.png "")

Quand vous aurez terminé, cela vous devriez voir s'afficher quelque chose comme ceci 
![TBD](images/cplm_addon-coopalim_enable-success.png "")

## [2/4] <PREPARATION DES DONNEES>
### Dupliquer le document Google Sheets ayant servi aux commandes originales
Il est important de toujours travailler avec une copie des données pour ne pas risquer d'affecter le document original
> Menu : File > Make a copy
![TBD](images/cplm_orders_duplicate-sheet.png "")

### Activer l'add-on Coopalim dans ce document
> Menu : Add-ons > Manage add-ons...
![TBD](images/cplm_addon-coopalim_use-in-document.png "")

> Vérifiez que l'activation est effective en cliquant à nouveau sur "Manage"
![TBD](images/cplm_addon-coopalim_use-in-document-checked.png "")

> Raffraîchissez la page du navigateur où vous vous trouvez avec le document ouvert

> Vérifiez que le menu de l'add-on apparaît bien à présent
![TBD](images/cplm_addon-coopalim_menu.png "")

### Exporter les données de facturation via la copie du document de commandes
> Menu : Add-ons > Coopalim : Google Sheets Utils > Export Facturation
![TBD](images/cplm_addon-coopalim_export-invoice-data.png "")

Cela génère un nouvelle feuille avec un nom tel que "facturation-<horodatage>"
Il s'agit des données prêtes à être utilisée pour la génération. 
Chaque ligne contient la commande d'un adhérent.

## [3/4] <INSTALLATION DE L'ADDON AUTOCRAT>
### Aller sur cette page chrome.google.com/webstore/detail/autocrat/ppgnklghfnlijoafjjkpoakpjjpdkgdj?utm_source=permalink
et installer l'addon. Il demande pas mal de permissions mais elles sont nécéssaires pour faire ce qu'il a à faire.

## [4/4] <CONFIGURATION DU JOB DE GENERATION VIA AUTOCRAT>
Suivez les étapes détaillées dans les captures d'écran ci-après
![TBD](images/cplm_addon-autocrat_configure_01.png "")

![TBD](images/cplm_addon-autocrat_configure_02.png "")

![TBD](images/cplm_addon-autocrat_job_01.png "")

![TBD](images/cplm_addon-autocrat_job_02-1.png "")

> Utiliser le fichier de template suivant https://goo.gl/5vVXWM (compatible avec l'add-on
![TBD](images/cplm_addon-autocrat_job_02-2.png "")

![TBD](images/cplm_addon-autocrat_job_03.png "")

![TBD](images/cplm_addon-autocrat_job_04.png "")

![TBD](images/cplm_addon-autocrat_job_05.png "")

![TBD](images/cplm_addon-autocrat_job_06.png "")

![TBD](images/cplm_addon-autocrat_job_07.png "")

![TBD](images/cplm_addon-autocrat_job_08.png "")

![TBD](images/cplm_addon-autocrat_job_09.png "")

![TBD](images/cplm_addon-autocrat_run-job.png "")



# Voir aussi / Ressources associées / Pour aller plus loin
 * https://gsuite.google.com/marketplace/app/autocrat/539341275670
 * https://sites.google.com/site/heroscripts/communication/autocrat
 * http://www.ticeman.fr/lecoutelas/?p=1481
 * Notice d'utilisation du format markdown [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
 * Fiche pratique de [Site du Zéro](http://www.siteduzero.com)
