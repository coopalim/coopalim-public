# [Fiche Pratique] Gérer les campagnes de commande

[//]: # (Version Canevas : 0.2)

# Description

Dans cette fiche pratique, vous allez apprendre à :
 * ...
 * ...

```mermaid
graph TD
	Debut_vente[Début de la campagne] --> lien(Distribution lien)
	lien --> Commande_Ouverture
	subgraph Commandes
		Commande_Ouverture(Ouvrir) --> Commande_Cloture(Clôturer)

	end
	subgraph Ajustements
		Commande_Cloture --> Ajuster[Ajuster les quantités à commander]
		Ajustements_cloturer --> Ajustements_Imprimer(Imprimer bons de commande)
		Ajustements_cloturer --> Ajustements_ImprimerAchats(Imprimer demandes achat)
		Ajuster --> Ajustements_cloturer(Clôturer)
	end
	subgraph Vente
		Ajustements_cloturer --> Vente_ouvrir[Ajuster les quantités délivrées]
		Vente_ouvrir --> Vente_cloturer(Clôturer)
		Vente_cloturer --> Vente_Imprimer(Imprimer facture)

	end
	Vente_cloturer --> Fin[Fin de la campagne]
```
 
## Prérequis

 * Avoir des connaissances de base en utilisation d'internet
 * Avoir les droits pour utiliser le menu **coopalim**

# Procédure

Suite à l'expérience de la première commande, il faudrait modifier l'architecture du menu **coopalim** :
 1. Menu **Obtenir le lien**:
  * Permet d'avoir le lien raccourci de la commande actuelle
 1. Menu **Commande -> Ouvrir**:
  * Pose les droits d'écriture pour tous.tes dans certains champs seulement
  * Pose les formules de vérifications spécifiques à chaque article, pour tous les membres. Ceci doit pouvoir être fait à tout moment (ajout de nouveaux membres).
 2. Menu **Commande -> Clôturer**:
  * Enlève les droits d'écriture sur la feuille commande
  * Copie la feuille **Commande** dans un nouvel onglet, protégé pour tous sauf pour des utilisateur.trice.s approuvé.e.s (ajustements de lots, etc)
 3. Menu **Ajustements -> Clôturer**:
  * Copie la feuille d'ajustement pour préparer la vente
    * La marge coopérative doit être remplacée par la différence entre ce que l'utilisateur voulait
  * Enlève les droits d'écriture sur la feuille d'ajustement
  * Autorise l'impression des **bons de commande** (**Ajustements -> Imprimer bons de commandes**)
 4. Menu **Vente --> Ouvrir**
  * Enlève provisoirement les protections durant la vente, dans le nouvel onglet
 5. Menu **Vente --> Clôturer**
  * Remet toutes les protections sur l'onglet
 6. Menu **Vente --> Imprimer factures**
 7. Menu **Lancer la prochaine commande**
  * Créer une copie de la campagne actuelle, sans les onglets générés aux étapes intermédiaires

# Voir aussi / Ressources associées / Pour aller plus loin

 * Lien internet pour la création d'un addon google-sheets ?