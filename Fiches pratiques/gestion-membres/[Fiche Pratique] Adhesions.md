# [Fiche Pratique] Adhésions

[//]: # (Version Canevas : 0.2)

## Description

Dans cette fiche pratique, vous allez apprendre à :

- Consulter la liste des adhésions référencées dans l'application
- Afficher le détail des informations d'une adhésion
- Rattacher les adhésions pour lesquelles aucun membre correspondant n'a été trouvé automatiquement (adhésions orphelines)

## Prérequis

TBD.

## Procédure

### Consulter la liste des adhésions

1. Via le menu de l'application, **cliquer sur "Membres" puis "Adhésions"**
2. Il est également possible de **filtrer les resultats** et faire des recherches selon plusieurs critères

![TBD](images/adhesions_list.png "")

### Afficher le détail des informations d'une adhésion

![TBD](images/adhesions_detail.png "")

### Rattacher une adhésion orpheline

1. Accéder à la **liste des adhésions** et
2. **Filtrer la liste** pour n'afficher que les adhésions orphelines
3. **Afficher le détail** de l'adhésion à rattacher
4. **Rechercher** un compte membre correspondant et le sélectionner
5. **Enregistrer le rattachement**

![TBD](images/adhesions_attach.png "")

## Voir aussi / Ressources associées / Pour aller plus loin

- Notice d'utilisation du format markdown [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- Fiche pratique de [Site du Zéro](http://www.siteduzero.com)