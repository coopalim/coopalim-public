# [Fiche Pratique] Application de gestion des membres/adhérents

[//]: # (Version Canevas : 0.2)

## Description

- L'application est accessible via un navigateur web à l'adresse [url_application]
- Les informations de connexion (identifiant et mot de passe) vous sont fournies par les référents Atelier Informatique.
- Si vous n'avez pas vos informations de connexion, merci d'envoyer un message à [informatique.coopalim@framalistes.org](mailto:informatique.coopalim@framalistes.org) 
avec comme sujet "Demande d'accès gestionnaire espace membres"

## Présentation

![TBD](images/tableau_de_bord.png "")

## Prérequis

- Connaître l'url d'accès à l'application
- Disposer d'un ordinateur de bureau ou mobile avec accès internet
- Disposer d'identifiants d'accès à l'application

## Procédures

1. [Gestion des adhésions]([Fiche%20Pratique]%20Adhesions.md)

2. [Consultation des actions HelloAsso]([Fiche%20Pratique]%20HelloAsso.md)

3. [Gestion des comptes membres]([Fiche%20Pratique]%20Membres.md)

## Voir aussi / Ressources associées / Pour aller plus loin

RAS.

- Notice d'utilisation du format markdown [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- Fiche pratique de [Site du Zéro](http://www.siteduzero.com)