# [Fiche Pratique] Membres

[//]: # (Version Canevas : 0.3)

## Description

Dans cette fiche pratique, vous allez apprendre à :

- Consulter la liste des comptes adhérents
- Afficher le détail d'un compte adhérent
- Mettre à jour les informations principales d'un adhérent
- Mettre à jour  les adhésions d'un adhérent
- Mettre à jour  les groupes/ateliers d'un adhérent
- Valider la fiche membre d'un adhérent
- Fusionner deux fiches membres

## Prérequis

TBD.

## Procédures

### Consulter la liste des comptes adhérents

### Afficher les détails d'un compte adhérent

1. Afficher les informations principales

![TBD](images/membres_detail_informations-principales.png "")

2. Afficher les adhésions d'un adhérent

![TBD](images/membres_detail_adhesions.png "")

3. Afficher les groupes/ateliers d'un adhérent

![TBD](images/membres_detail_groups.png "")

### Modifier les informations principales d'un adhérent

### Modifier les adhésions d'un adhérent

### Modifier les groupes/ateliers d'un adhérent

### Valider la fiche membre d'un adhérent

### Fusionner deux fiches membres

## Voir aussi / Ressources associées / Pour aller plus loin

- Notice d'utilisation du format markdown [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- Fiche pratique de [Site du Zéro](http://www.siteduzero.com)