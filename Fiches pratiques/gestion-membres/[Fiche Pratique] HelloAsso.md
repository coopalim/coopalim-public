# [Fiche Pratique] HelloAsso

[//]: # (Version Canevas : 0.2)

## Description

Dans cette fiche pratique, vous allez apprendre à :
 * Consulter la liste des actions HelloAsso
 * Afficher le détail d'une action HelloAsso

L'application est connectée au [Compte HelloAsso de Coopalim](https://www.helloasso.com/associations/association-coopalim-de-strasbourg/adhesions/coopalim-2020-2021).
Toutes les adhésions effectuées via HelloAsso sont donc régulièrement synchronisées de HelloAsso vers l'application.

## Prérequis
TBD.

## Procédure

### Consulter la liste des actions HelloAsso

![TBD](images/helloasso_actions-list.png "")

### Afficher le détail d'une action HelloAsso

![TBD](images/helloasso_actions-detail.png "")

## Voir aussi / Ressources associées / Pour aller plus loin

 * Notice d'utilisation du format markdown [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
 * Fiche pratique de [Site du Zéro](http://www.siteduzero.com)