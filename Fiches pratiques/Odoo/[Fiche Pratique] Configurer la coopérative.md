# [Fiche Pratique] Configurer l'entreprise

[//]: # (Version Canevas : 0.2)

# Description

Dans cette fiche pratique, vous allez apprendre à configurer les caractéristiques de l'entreprise/association/coopérative, comme l'adresse, le site internet, etc. 

Ces informations sont importantes, puisqu'elles sont utilisées sur les factures à l'attention des clients.

## Prérequis

 * Il faut être administrateur Odoo pour effectuer cette manipulation.
 
# Procédure

 1. Cliquer sur **configuration**
 2. Cliquer sur **Setup**

![Pour aller dans l'interface d'édition](images/ConfigMonEntreprise1.png "Pour aller dans l'interface d'édition")

Puis dans la nouvelle interface:
 1. Editer le nom de l'entreprise/association/coopérative;
 2. L'adresse;
 3. Le site internet;
 4. le courriel;
 5. Finalement Cliquer sur **Sauvegarder**;
 
Evidemment, il est possible de renseigner les autres champs...
 
 ![Champs à éditer](images/ConfigMonEntreprise2.png "Interface d'édition")

# Voir aussi / Ressources associées / Pour aller plus loin

 * [(Fiche Pratique) Faire une facture d'acompte]([Fiche Pratique] Faire une facture d acompte.md)
 * [(Fiche Pratique) Faire une facture de solde de tout compte]([Fiche Pratique] Faire une facture de solde de tout compte.md)