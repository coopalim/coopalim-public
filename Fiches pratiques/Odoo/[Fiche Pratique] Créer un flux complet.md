# [Fiche Pratique] Créer un flux complet

[//]: # (Version Canevas : 0.2)

# Description

Dans cette fiche pratique, vous allez apprendre à générer un flux complet, de la vente à la livraison finale (par exemple la distribution), qui donnerait le flux suivant:
```mermaid
graph TD
	VENTE(Faire une vente) --> |Validation| ACOMPTE[Facturer éventuellement l'acompte] 
	ACOMPTE --> |Règlement encaissé| PLANIFICATION{Planifier,assez de stock?}
	PLANIFICATION --> |Oui| CONDITIONNEMENT{bon conditionnement?}
	PLANIFICATION --> |Non| ACHAT(Faire la demande d'achat fournisseur)
	
	CONDITIONNEMENT --> |oui| LIVRAISON(Livrer, ajuster les quantités vendues)
	ACHAT -->|Produits disponibles| RECEPTION(Réceptionner, contrôler les produits reçus)
	RECEPTION --> REGLEMENT[Régler le fournisseur]
	REGLEMENT --> CONDITIONNEMENT
	
	CONDITIONNEMENT -->|non| FABRICATION(Transformer en vrac)
	FABRICATION -->|Conditionnement en vrac| LIVRAISON
	
	LIVRAISON--> SOLDE[Facturer et encaisser le solde de tout compte]
```

## Prérequis :
 * [(Fiche Pratique) Configurer la coopérative]([Fiche Pratique] Configurer la coopérative.md)
 * [Fiche Pratique] Configurer l'entrepôt
 * [(Fiche Pratique) Configurer une catégorie]([Fiche Pratique] Configurer une catégorie.md)

# Procédure

Appliquer les fiches suivantes (dans cet ordre):
 1. [(Fiche Pratique) Ajouter un fournisseur]([Fiche Pratique] Configurer un fournisseur.md)
 2. [(Fiche Pratique) Configurer un article en vrac]([Fiche Pratique] Configurer un article en vrac.md)

Nota: Ce processus fonctionne dans les grandes lignes, mais doit être amélioré, car:
 * Il y a un peu trop de clics à faire (certaines opérations pourraient être optimisées)

# Pour aller plus loin :

Appliquer les fiches suivantes pour parcourir le processus (dans cet ordre):
 1. [(Fiche Pratique) Faire une vente]([Fiche Pratique] Faire une vente.md)
  * Optionnel : [(Fiche Pratique) Faire une facture d'acompte]([Fiche Pratique] Faire une facture d acompte.md)
 2. [(Fiche Pratique) Faire une planification]([Fiche Pratique] Planifier les demandes d achat.md)
 3. [(Fiche Pratique) Faire une demande d'achat]([Fiche Pratique] Faire une demande d achat.md)
  * Optionnel : [Fiche Pratique] Régler la demande d'achat
 4. [(Fiche Pratique) Faire une réception]([Fiche Pratique] Faire une réception.md)
 5. [(Fiche Pratique) Faire une transformation de gros vers vrac]([Fiche Pratique] Faire une transformation de gros vers vrac.md)
 6. [(Fiche Pratique) Faire une livraison]([Fiche Pratique] Faire une livraison.md)
  * Optionnel : [(Fiche Pratique) Faire une facture de solde de tout compte]([Fiche Pratique] Faire une facture de solde de tout compte.md)

## Ressources

