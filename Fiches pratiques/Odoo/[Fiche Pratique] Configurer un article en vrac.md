# [Fiche Pratique] Configurer un article en vrac

[//]: # (Version Canevas : 0.2)

# Description

Cette fiche permet d'ajouter un article de type vrac (vente au kilo, au gramme). Celui-ci sera **produit** nécessairement à partir d'un lot conditionné. En effet, les produits en vrac sont vendus par lots de plus ou moins grande quantités par des grossistes.

**IMPORTANT : L'ensemble des articles vendus par lot et revendus par sous-unités sont concernés (des oeufs par exemple).**

Un exemple, avec des clous:
```mermaid
graph TD
    GROS(Un lot conditionné par 20kg) -->|nomenclature|TRANSFORMATION(Dé-conditionnement)
	TRANSFORMATION --> VRAC(20 kg de vrac)
	VRAC --> VENTE(Vente à la pesée)
```

## Prérequis

Avoir configuré les catégories **vrac** et **gros**:
 * [(Fiche Pratique) Configurer une catégorie]([Fiche Pratique] Configurer une catégorie.md)

# Procédure

## Configurer un lot conditionné

![TBD](images/AjoutArticleGros1.png "TBD")

![TBD](images/AjoutArticleGros2.png "TBD")

![TBD](images/AjoutArticleGros3.png "TBD")

![TBD](images/AjoutArticleGros4.png "TBD")

## Configurer un produit en vrac

![TBD](images/AjoutArticleVrac1.png "TBD")

![TBD](images/AjoutArticleVrac2.png "TBD")
 
## Configurer une nomenclature

![TBD](images/AjoutNomenclature1.png "TBD")

![TBD](images/AjoutNomenclature2.png "TBD")

![TBD](images/AjoutNomenclature3.png "TBD")

## Régler les seuils d'approvisionnement

Il faut faire ceci pour les deux articles:
 * [(Fiche Pratique) Configurer les seuils d'approvisionnement]([Fiche Pratique] Configurer les seuils d approvisionnenement.md)

# Voir aussi / Ressources associées / Pour aller plus loin

 * https://www.odoo.com/documentation/user/11.0/fr/manufacturing/management/bill_configuration.html#setting-up-a-basic-bom
 * https://www.odoo.com/documentation/user/11.0/fr/manufacturing/management/manufacturing_order.html#how-to-manage-manufacturing-without-routings