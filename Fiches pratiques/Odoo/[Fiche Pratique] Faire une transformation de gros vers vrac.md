# [Fiche Pratique] Faire une transformation de gros vers vrac

[//]: # (Version Canevas : 0.2)

# Description

Dans cette fiche pratique, vous allez apprendre à :
 * ...
 * ...

## Prérequis

TBD

# Procédure

![TBD](images/FabricationVrac1.png "")

![TBD](images/FabricationVrac2.png "")

# Voir aussi / Ressources associées / Pour aller plus loin

TBD