# [Fiche Pratique] <TITRE DE LA FICHE PRATIQUE, sans le .md>

[//]: # (Version Canevas : 0.2)

# Description

Dans cette fiche pratique, vous allez apprendre à paramétrer Odoo, afin de définir des seuils haut et bas afin que le planificateur puisse effectuer les calculs nécessaires.

En effet, le système est capable de vous aider à maintenir les stocks dans les limites nécessaires, en faisant par exemple des commandes fournisseurs et/ou des ordres de fabrication.

Le processus est le suivant (à confirmer):
```mermaid
graph TD
	OPER1(Faire une vente) -->|Validation| STOCKMIN{Stock inférieur à limite basse?}
	OPER2(Faire une vente) -->|Validation| STOCKMIN
	OPER3(Faire une production) -->|Validation| STOCKMIN
	
	STOCKMIN --> |Non| FIN((Fin))

	STOCKMIN --> |Oui| COMPROD[Commander / produire multiple de ...]
	COMPROD --> STOCKMAX{Stock maximum atteint}

	STOCKMAX--> |Oui| FIN
	STOCKMAX--> |non| COMPROD
```

## Prérequis

Il faut avoir avoir au préalable avoir configuré des produits:
 * [(Fiche Pratique) Configurer un article en vrac]([Fiche Pratique] Configurer un article en vrac.md)

# Procédure

Il faut suivre la procédure suivante:
 1. Cliquer sur **inventaire**;
 2. Cliquer sur **règles de réapprovisionnement**;
 3. Sélectionner l'article concerné , ou en créer un nouveau et sélectionner l'article dans la liste;
 4. Appliquer l'une des stratégies ci-dessous, selon les besoins;
 5. Cliquer sur **Sauvegarder**.
 
![Seuils d'approvisionnement](images/SeuilsAppro.png "Paramétrage des seuils d'approvisionnement")

## Stratégie par défaut

Cette stratégie est très simple. Le système ne va générer que le strict minimum en terme de commandes et/ou d'ordres de productions, pour satisfaire les ventes, sans avoir de stock. Il est à la charge des personnes ensuite de corriger les demandes d'achats et/ou les ordres de productions (voir fiches dédiées).

Paramètres:
 * **Stock minimum :** 0
 * **Stock maximum :** 0
 * **Multiples de :** 0

## Stratégies lots entiers

Cette stratégie correspond bien a la commande de lots. Ainsi, par exemple pour un produit appelé **lotsClous5kg** (conditionnement fournisseur par 5kg), on configurera le système ainsi:
 * **Multiples de :** 1
 
Ceci indique au système qu'il ne commandera/produira que des lots **entiers** de 5kg.

Avec un tel paramétrage, **seules les demandes multiples de cette valeur seront satisfaites**. 

# Voir aussi / Ressources associées / Pour aller plus loin

 * [(Fiche Pratique) Faire une demande d'achat]([Fiche Pratique] Faire une demande d achat.md)
 * [(Fiche Pratique) Faire une transformation de gros vers vrac]([Fiche Pratique] Faire une transformation de gros vers vrac.md)
 * [(Fiche Pratique) Planifier les demandes d'achat]([Fiche Pratique] Planifier les demandes d achat.md)
 * [Règles d'approvisionnement sur le site Odoo](http://www.odoo.com/documentation/user/11.0/fr/inventory/settings/products/strategies.html#minimum-stock-rule)
 * [Autre exemple sur le site Odoo](http://www.odoo.com/documentation/user/11.0/fr/purchase/replenishment/flows/setup_stock_rule.html)