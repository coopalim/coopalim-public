# [Fiche Pratique] Evaluer des demandes d'achat

[//]: # (Version Canevas : 0.2)

# Description

Dans cette fiche pratique, vous allez apprendre à génerer des demandes d'achat préremplies par Odoo, grâce à un système appelé le **planificateur**.

Celui-ci se fait en une seule étape, mais en réalité met en place plusieurs processus:
 * Chaque vente va créer un flux de produit, présent ou futur
 * Le stock va donc fluctuer, et donc se retrouver en dehors de limites hautes et basses
 * Si c'est le stock passe en dessous de la limite basse, Odoo va faire une demande d'achat pour atteindre la limite haute, grâce aux **règles d'approvisionnement**, spécifiques à chaque produit.
 * Il n'y a plus ensuite qu'à effectuer la demande d'achat.

Ce qui peut être synthétisé par le schéma suivant:
```mermaid
graph TD
	VENTE1(Faire une vente) -->|Validation| EVALUATION{Stock supérieur à limite basse?}
	VENTE2(Faire une vente) -->|Validation| EVALUATION
	VENTE3(Faire une vente) -->|Validation| EVALUATION
	
	EVALUATION --> |Oui| LIVRAISON(Livraison - ajustements des quantités données)
	EVALUATION --> |Non| ACHAT(Acheter/produire jusqu'à la limite haute)
```


## Prérequis

Avoir ajouté les produits en suivant les procédures suivantes:
 * [(Fiche Pratique) Configurer un article en vrac([Fiche Pratique] Configurer un article en vrac.md)
 * [(Fiche Pratique) Régler les seuils d'approvisionnement]([Fiche Pratique] Configurer les seuils d approvisionnenement.md)
 
# Procédure

 1. Aller sur **inventaire**
 2. Cliquer sur **Lancer le planificateur**
 3. Valider en cliquant sur **Lancer les planificateurs**
 
Les éventuelles demande d'achat sont créées, il faut donc consulter:
 * [(Fiche Pratique) Faire une demande d'achat]([Fiche Pratique] Faire une demande d achat.md)

![Aller dans la planification](images/Planification1.png "Aller dans la planification")

![Lancer la planification](images/Planification2.png "Lancer la planification")

# Voir aussi / Ressources associées / Pour aller plus loin

 * [Les planificateurs sur le site Odoo](https://www.odoo.com/documentation/user/11.0/fr/inventory/management/misc/schedulers.html)