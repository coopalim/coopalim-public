# [Fiche Pratique] <TITRE DE LA FICHE PRATIQUE, sans le .md>

[//]: # (Version Canevas : 0.3)

## Description

Dans cette fiche pratique, vous allez apprendre à :

- ...
- ...

## Prérequis

- [Fiche Pratique] Configurer son entreprise

Fiches pratiques *EXCEL* : <Doit correspondre au répertoire de base où trouver le document>
- [Fiche Pratique] Comment créer un fichier CSV depuis Excel

Fiches pratiques *Facturation* : <Doit correspondre au répertoire de base où trouver le document>
 
## Procédure

<SI POSSIBLE>

### [1/3] <TITRE DE L'OPERATION>

#### <Sous opération 1>

-  

#### ...

### [2/3] <TITRE DE L'OPERATION>

### [3/3] <TITRE DE L'OPERATION>

## Voir aussi / Ressources associées / Pour aller plus loin

<NE METTRE ICI QUE CE QUI EST STRICTEMENT NECESSAIRE. LE TEXT CI-APRES EST FOURNI à TITRE D'EXEMPLE>

- Notice d'utilisation du format markdown [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- Fiche pratique de [Site du Zéro](http://www.siteduzero.com)