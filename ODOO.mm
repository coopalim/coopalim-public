<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1517572723862" ID="ID_1874475146" MODIFIED="1517574238762" TEXT="ODOO">
<hook NAME="accessories/plugins/HierarchicalIcons.properties"/>
<node CREATED="1517572858597" FOLDED="true" ID="ID_1543944849" MODIFIED="1517579201231" POSITION="left" TEXT="Sales">
<icon BUILTIN="full-1"/>
<node CREATED="1517575652068" FOLDED="true" ID="ID_537432632" MODIFIED="1517579198851" TEXT="Ajouter des conditions de vente (variation de prix)">
<icon BUILTIN="password"/>
<node CREATED="1517575671048" ID="ID_105141594" MODIFIED="1517575671958" TEXT="https://www.odoo.com/documentation/user/11.0/fr/sales/quotation/setup/terms_conditions.html#general-terms-and-conditions"/>
</node>
<node CREATED="1517573900165" FOLDED="true" ID="ID_358958826" MODIFIED="1517574048120" TEXT="Afficher les prix TTC par d&#xe9;faut">
<icon BUILTIN="password"/>
<icon BUILTIN="full-1"/>
<node CREATED="1517573911795" ID="ID_1411520020" MODIFIED="1517573912715" TEXT="https://www.odoo.com/documentation/user/11.0/accounting/others/taxes/tax_included.html#show-tax-included-prices-in-ecommerce-catalog"/>
</node>
</node>
<node CREATED="1517572842437" ID="ID_1865803439" MODIFIED="1517574569649" POSITION="left" TEXT="Inventaire (Inventory)">
<icon BUILTIN="full-2"/>
<node CREATED="1517575147368" FOLDED="true" ID="ID_599897875" MODIFIED="1517579278642" TEXT="Automatisation des demandes d&apos;approvisions">
<node CREATED="1517575095047" FOLDED="true" ID="ID_758312650" MODIFIED="1517579273792" TEXT="Configurer pour chaque article une r&#xe8;gle de stock minimum">
<icon BUILTIN="full-2"/>
<node CREATED="1517575089287" ID="ID_45012458" MODIFIED="1517575091507" TEXT="https://www.odoo.com/documentation/user/11.0/fr/inventory/management/adjustment/min_stock_rule_vs_mto.html#minimum-stock-rules"/>
<node CREATED="1517578236892" FOLDED="true" ID="ID_57372944" MODIFIED="1517579272092" TEXT="Pour un groupement d&apos;achat :">
<node CREATED="1517578256872" ID="ID_42373908" MODIFIED="1517578409873" TEXT="Stock minimum = 0">
<icon BUILTIN="password"/>
</node>
<node CREATED="1517578270522" ID="ID_779157812" MODIFIED="1517578419513" TEXT="Stock maximum = 0">
<icon BUILTIN="password"/>
</node>
<node CREATED="1517578284772" ID="ID_410069898" MODIFIED="1517578424973" TEXT="Unit&#xe9; = 1 (toujours, car les fournisseurs ne vendent pas de demi-lots)">
<icon BUILTIN="password"/>
<node CREATED="1517578381543" ID="ID_17377766" MODIFIED="1517578392883" TEXT="Voir aussi transformation"/>
</node>
</node>
<node CREATED="1517578236892" FOLDED="true" ID="ID_1127329956" MODIFIED="1517579270092" TEXT="[A essayer] Pour un groupement d&apos;achat :">
<icon BUILTIN="idea"/>
<node CREATED="1517578256872" ID="ID_580324943" MODIFIED="1517578409873" TEXT="Stock minimum = 0">
<icon BUILTIN="password"/>
</node>
<node CREATED="1517578270522" ID="ID_55347419" MODIFIED="1517578419513" TEXT="Stock maximum = 0">
<icon BUILTIN="password"/>
</node>
<node CREATED="1517578284772" ID="ID_949375970" MODIFIED="1517579267772" TEXT="Unit&#xe9; = &lt;unit&#xe9; de conditionnement du fournisseur&gt; (toujours, car les fournisseurs ne vendent pas de demi-lots)">
<icon BUILTIN="password"/>
<node CREATED="1517578381543" ID="ID_866607683" MODIFIED="1517578392883" TEXT="Voir aussi transformation"/>
</node>
</node>
</node>
<node CREATED="1517575194488" ID="ID_896080762" MODIFIED="1517575197278" TEXT="ou"/>
<node CREATED="1517574887734" FOLDED="true" ID="ID_40687819" MODIFIED="1517579277312" TEXT="Configurer chaque article en approvisionnement &#xe0; la commande">
<icon BUILTIN="password"/>
<icon BUILTIN="full-2"/>
<node CREATED="1517575005605" ID="ID_422151643" MODIFIED="1517575006715" TEXT="https://www.odoo.com/documentation/user/11.0/fr/inventory/management/adjustment/min_stock_rule_vs_mto.html#id1"/>
</node>
</node>
</node>
<node CREATED="1517572834117" ID="ID_339319794" MODIFIED="1517574624355" POSITION="left" TEXT="Gestion des achats (purchase management)">
<icon BUILTIN="full-3"/>
</node>
<node CREATED="1517572782964" ID="ID_1214100540" MODIFIED="1517574553119" POSITION="left" TEXT="Comptabilit&#xe9; (Accounting)">
<icon BUILTIN="full-4"/>
<node CREATED="1517576794363" FOLDED="true" ID="ID_248278048" MODIFIED="1517579212061" TEXT="Faire un encaissement">
<icon BUILTIN="full-4"/>
<node CREATED="1517576788353" ID="ID_1874179543" MODIFIED="1517576789793" TEXT="https://www.odoo.com/documentation/user/11.0/fr/accounting/receivables/customer_payments/recording.html#paying-an-invoice"/>
</node>
<node CREATED="1517573820454" FOLDED="true" ID="ID_1440312038" MODIFIED="1517574049710" TEXT="D&#xe9;finir une politique de prix taxes incluses (B2C)">
<icon BUILTIN="password"/>
<icon BUILTIN="full-4"/>
<node CREATED="1517573815084" ID="ID_323413990" MODIFIED="1517573816644" TEXT="https://www.odoo.com/documentation/user/11.0/accounting/others/taxes/tax_included.html"/>
</node>
</node>
<node CREATED="1517572823156" FOLDED="true" ID="ID_1139186462" MODIFIED="1517581933090" POSITION="left" TEXT="Manufacturing">
<icon BUILTIN="full-5"/>
<node CREATED="1517579106400" FOLDED="true" ID="ID_414214125" MODIFIED="1517580534173" TEXT="D&#xe9;conditionner les lots fournisseurs">
<icon BUILTIN="full-5"/>
<node CREATED="1517578959319" ID="ID_129912349" MODIFIED="1517578962189" TEXT="https://www.odoo.com/documentation/user/11.0/fr/manufacturing/management/manufacturing_order.html#how-to-manage-manufacturing-without-routings"/>
</node>
<node CREATED="1517580590766" FOLDED="true" ID="ID_242480742" MODIFIED="1517581931220" TEXT="Cr&#xe9;er une r&#xe8;gle de d&#xe9;conditionnement fournisseur">
<arrowlink DESTINATION="ID_242480742" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_292871286" STARTARROW="None" STARTINCLINATION="0;0;"/>
<linktarget COLOR="#b0b0b0" DESTINATION="ID_242480742" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_292871286" SOURCE="ID_242480742" STARTARROW="None" STARTINCLINATION="0;0;"/>
<icon BUILTIN="full-5"/>
<node CREATED="1517580618417" ID="ID_811318341" MODIFIED="1517580619417" TEXT="https://www.odoo.com/documentation/user/11.0/fr/manufacturing/management/bill_configuration.html#setting-up-a-basic-bom"/>
<node CREATED="1517580840593" ID="ID_1787393567" MODIFIED="1517580853883" TEXT="Cr&#xe9;er un lot fournisseur Lot5"/>
<node CREATED="1517580642638" ID="ID_1253829788" MODIFIED="1517580749821" TEXT="S&#xe9;lectionner le vrac, saisir le nombre d&apos;unit&#xe9;s &quot;d&#xe9;conditionn&#xe9;es&quot; 5 pour un lot de 5"/>
</node>
</node>
<node CREATED="1517572946230" ID="ID_1466796477" MODIFIED="1517576462793" POSITION="right" TEXT="Processus g&#xe9;n&#xe9;ral">
<node CREATED="1517573392816" ID="ID_340969601" MODIFIED="1517575730858" TEXT="https://www.odoo.com/documentation/user/11.0/fr/accounting/receivables/customer_invoices/overview.html#sales-order-delivery-order-invoice"/>
<node CREATED="1517573517258" ID="ID_1061476221" MODIFIED="1517574339304" TEXT="Invoice based on delivered quantity">
<icon BUILTIN="password"/>
<icon BUILTIN="help"/>
</node>
<node CREATED="1517572928008" ID="ID_569315919" MODIFIED="1517578698054" TEXT="Devis (Quotation) / Bon de commande (invoice)">
<icon BUILTIN="full-1"/>
</node>
<node CREATED="1517575933582" ID="ID_1651972972" MODIFIED="1517579171951" TEXT="(optionnel) Acompte">
<icon BUILTIN="full-4"/>
<node CREATED="1517577995448" ID="ID_1783882138" LINK="#ID_1977618169" MODIFIED="1517578713574" TEXT="Automatisation possible"/>
<node CREATED="1517579179421" ID="ID_366410306" MODIFIED="1517579188811" TEXT="Manuellement"/>
</node>
<node CREATED="1517578977499" ID="ID_524652515" LINK="#ID_248278048" MODIFIED="1517579049710" TEXT="(optionnel) Encaissement de l&apos;acompte">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1517573013120" FOLDED="true" ID="ID_681177161" MODIFIED="1517580499702" TEXT="Demande approvisionnement">
<node CREATED="1517575431324" ID="ID_930717861" LINK="#ID_599897875" MODIFIED="1517576340195" TEXT="Automatiquement">
<icon BUILTIN="full-2"/>
</node>
<node CREATED="1517575439165" ID="ID_1556003706" LINK="#ID_1020683557" MODIFIED="1517576327795" TEXT="Manuellement">
<icon BUILTIN="full-3"/>
</node>
</node>
<node CREATED="1517575899981" FOLDED="true" ID="ID_994364393" MODIFIED="1517580502452" TEXT="R&#xe9;ception">
<icon BUILTIN="full-2"/>
<node CREATED="1517576394397" ID="ID_255749182" MODIFIED="1517576396277" TEXT="https://www.odoo.com/documentation/user/11.0/fr/inventory/overview/process/sale_to_delivery.html#retrieve-the-receipt"/>
</node>
<node CREATED="1517575874361" ID="ID_222324998" LINK="#ID_414214125" MODIFIED="1517580522763" TEXT="D&#xe9;conditionnement des lots fournisseurs">
<icon BUILTIN="full-5"/>
</node>
<node CREATED="1517573436337" ID="ID_249756131" MODIFIED="1517573488617" TEXT="Livraison (Delivery)">
<icon BUILTIN="full-2"/>
<node CREATED="1517576523805" ID="ID_1179233800" MODIFIED="1517576525555" TEXT="https://www.odoo.com/documentation/user/11.0/fr/inventory/overview/process/sale_to_delivery.html#retrieve-the-delivery-order"/>
</node>
<node CREATED="1517575925781" ID="ID_249334541" MODIFIED="1517575993352" TEXT="Facturation">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1517578977499" ID="ID_1797225808" LINK="#ID_248278048" MODIFIED="1517579392546" TEXT="Encaissement du solde">
<icon BUILTIN="full-4"/>
</node>
</node>
<node CREATED="1517573658603" FOLDED="true" ID="ID_251133401" MODIFIED="1517579205461" POSITION="left" TEXT="Modules sp&#xe9;cifiques">
<node CREATED="1517573647593" FOLDED="true" ID="ID_15632986" MODIFIED="1517574526608" TEXT="Adh&#xe9;sion : facture vos adh&#xe9;rents chaque ann&#xe9;e">
<node CREATED="1517574497409" ID="ID_456698636" MODIFIED="1517574507729" TEXT="https://www.odoo.com/documentation/user/11.0/fr/accounting/receivables/customer_invoices/overview.html#specific-modules">
<icon BUILTIN="info"/>
</node>
</node>
</node>
<node CREATED="1517574093603" ID="ID_1171984621" MODIFIED="1517574098423" POSITION="right" TEXT="Cycle de facturation">
<node CREATED="1517578030448" FOLDED="true" ID="ID_1977618169" MODIFIED="1517578667284" TEXT="Mettre en place des modalit&#xe9;s de paiement automatiquement">
<icon BUILTIN="full-4"/>
<node CREATED="1517578021268" ID="ID_1301026533" MODIFIED="1517578022208" TEXT="https://www.odoo.com/documentation/user/11.0/fr/accounting/receivables/customer_invoices/installment_plans.html#payment-terms-for-customers"/>
</node>
<node CREATED="1517574101053" ID="ID_1018176837" MODIFIED="1517574267023" TEXT="Du bon de commande vers paiement">
<icon BUILTIN="full-4"/>
</node>
<node CREATED="1517574140556" ID="ID_1064692979" MODIFIED="1517574210531" TEXT="De la facture fournisseur au paiement">
<icon BUILTIN="full-4"/>
<node CREATED="1517574193520" ID="ID_375292647" MODIFIED="1517574194350" TEXT="https://www.odoo.com/documentation/user/11.0/accounting/overview/process_overview/supplier_bill.html"/>
</node>
</node>
<node CREATED="1517578015128" ID="ID_458772884" MODIFIED="1517578018878" POSITION="left" TEXT="Comptabilit&#xe9;"/>
</node>
</map>
