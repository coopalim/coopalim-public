# [Fiche Pratique] Installer Odoo via un Docker

[//]: # (Version Canevas : 0.2)

# Description

Dans cette fiche pratique, vous allez apprendre à :
 * Démarrer une installation fraîche de Odoo 11.0 avec Postgresl 9.4

## Prérequis
 * [Installer git sur sa machine](https://openclassrooms.com/courses/gerer-son-code-avec-git-et-github/installer-git)
 * [Installer docker CE](https://docs.docker.com/install/#platform-support-matrix)
 * [Installer docker-compose](https://docs.docker.com/compose/install/)
 
# Procédure

 * Ouvrir un invité de commande (**cmd.exe** (Windows) ou terminal (Linux))
 * cloner le dépôt **coopalim-public** si nécessaire :
```
> git clone https://gitlab.com/coopalim/coopalim-public.git
```

 * Aller dans le bon répertoire :
```
> cd coopalim-public/Docker-odoo/
```

 * Démarrer les container en tâche de fond (option ```-d```) :
```
> docker-compose -f docker-compose.yml -d
```

 * récupérer le CONTAINER ID (identifiant) du container Odoo (ici ```ab46985bb152```) :
```
> docker ps
CONTAINER ID        IMAGE                                                        COMMAND                  CREATED             STATUS              PORTS                                                                     NAMES
ab46985bb152        odoo:11.0                                                    "/entrypoint.sh odoo"    24 minutes ago      Up About a minute   0.0.0.0:8069->8069/tcp, 8071/tcp                                          docker_coopalim-odoo_1
010aac016dc2        postgres:9.4                                                 "docker-entrypoint..."   24 minutes ago      Up About a minute   5432/tcp                                                                  docker_coopalim-db_1
```

 * à partir de la console, utiliser le CONTAINER ID pour récupérer l'adresse IP du container Odoo (ici ```172.21.0.3```) :
```
> docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ab46985bb152
172.21.0.3
```

 * Afficher l'application Odoo sur un navigateur web avec l'adresse IP trouvée précedemment :
```
https://172.21.0.3:8069
```

# Voir aussi / Ressources associées / Pour aller plus loin

 * [Initiation à Docker Compose](https://docs.docker.com/compose/overview/)
 * [Autre méthode pour utiliser Odoo à partir de Docker](https://unkkuri.com/blog/unkkuri-blog-1/post/install-odoo-version-11-in-docker-container-21)
