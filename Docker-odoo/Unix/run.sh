@ECHO OFF
REM https://unkkuri.com/blog/unkkuri-blog-1/post/install-odoo-version-11-in-docker-container-21

ECHO Next let's create and run the database container
docker run -d --name unkkuri-db --env POSTGRES_USER=odoo --env POSTGRES_PASSWORD=unkkuri-secret-pw --network=unkkuri-odoo-nw --mount source=unkkuri-db-data,target=/var/lib/postgresql/data  library/postgres:10.0
  
ECHO Checking POSTGRES logs

docker logs unkkuri-db

pause

docker run -d --name unkkuri-odoo --link unkkuri-db:db -p 8069:8069 --network unkkuri-odoo-nw --mount source=unkkuri-odoo-data,target=/var/lib/odoo --mount source=unkkuri-odoo-extra-addons,target=/mnt/extra-addons --env POSTGRES_PASSWORD=unkkuri-secret-pw library/odoo:11.0
 
ECHO Checking ODOO logs
docker logs unkkuri-odoo