@ECHO OFF
REM https://unkkuri.com/blog/unkkuri-blog-1/post/install-odoo-version-11-in-docker-container-21

ECHO Let's create a Docker network to connect our database and Odoo.
docker network create --driver bridge unkkuri-odoo-nw

ECHO CREATING a volume to persist database:
docker volume create --name unkkuri-db-data

ECHO CREATING Odoo volumes
docker volume create --name unkkuri-odoo-data
docker volume create --name unkkuri-odoo-extra-addons