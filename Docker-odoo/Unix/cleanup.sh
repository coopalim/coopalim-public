@ECHO 0FF
CALL stop.bat

docker rm unkkuri-odoo
docker volume rm unkkuri-odoo-data 
docker volume rm unkkuri-odoo-extra-addons

docker rm unkkuri-db
docker volume rm unkkuri-db-data

docker network rm unkkuri-odoo-nw
pause